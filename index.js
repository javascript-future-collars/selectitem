const selectItem = () => {
  const select = document.createElement("select");
  select.id = "level";
  for (let i = 1; i <= 3; i++) {
    const option = document.createElement("option");
    select.appendChild(option);
    option.innerText = i;
    option.value = i;
  }
  select.addEventListener("change", function (option) {
    alert(`wybrana opcja: ${option.target.value}`);
  });
  document.body.appendChild(select);
  return select;
};

selectItem();
